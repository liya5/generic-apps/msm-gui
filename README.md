# MSM GUI
MSM GUI, abbrevates to Manjaro Settings Manager Graphical User Interface is a tool to help you manage your kernels,
<br>
 keyboard layouts, your drivers, time zone, language packs and locale settings.
## Dependencies
Runtime :-
```
icu
qt5-base
hwinfo
kitemmodels
kauth
kcoreaddons
ckbcomp
xdg-utils
mhwd-arch
```
Make :-
```
git
cmake
extra-cmake-modules
kdoctools
qt5-tools
knotifications
kconfigwidgets
kcmutils
```
## TO COMPILE
Please follow the following commands to compile
``` git clone https://gitlab.com/liya5/msm-gui.git \\ Git cloning
    cd msm-gui
    mkdir build
    cd build
    cmake ../ \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DLIB_INSTALL_DIR=lib \
    -DKDE_INSTALL_USE_QT_SYS_PATHS=ON \
    -DSYSCONF_INSTALL_DIR=/etc
    sudo make
    sudo make install
```
